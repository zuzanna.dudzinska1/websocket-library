import { io } from "socket.io-client";

const tool = {
  throttle: function(func, interval) {
    var lastCall = 0;
    return function () {
      var now = Date.now();
      if (lastCall + interval < now) {
        lastCall = now;
        return func.apply(this, arguments);
      }
    };
  },
  qs:(selector, parent)=>{
    if (parent){
        return document.querySelector(parent).querySelector(selector);
    } else{
        return document.querySelector(selector);
    }
  },
  returnArray:(selector, parent)=>{
      if (parent){
          return [... document.querySelector(parent).querySelectorAll(selector)];
      } else{
          return [... document.querySelectorAll(selector)];
      }
  },
}

export const WebSocket = {
  socket: null,
  pixelConnect: null,
  pixelDisconnect: null,
  roomId: 0,
  config: function(pixelConnect, pixelDisconnect){
    this.pixelConnect = pixelConnect
    this.pixelDisconnect = pixelDisconnect

    WebSocket.socket = io("http://localhost:3000", {      
        withCredentials: false,
    });
  },
  connect: function(){
    return new Promise((resolve, reject) =>{
      WebSocket.socket.on('connect', function() {
        resolve();
      });
    })
  },
  joinRoom: function(roomId){
    this.roomId = roomId ? roomId : this.socket.id
    this.sendMessage("user_joinRoom")
  },
  sendMessage: function(type, message){
    this.socket.emit(type, {roomId: this.roomId, ...message})
  },
  getMessage: function(type, handleReciveMessage){
    this.socket.on(type, (message)=>{
      handleReciveMessage(message)
    })  
  }
}



