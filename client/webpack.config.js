const path = require('path');

module.exports = {
  entry: './src/main.js',
  output: {
    filename: 'main.js',    
    path: path.resolve(__dirname, 'dist'),    
    library: "satakuSocket",
    clean: true,
  },
  mode: process.env.NODE_ENV || 'development'
};
