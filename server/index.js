import { Server } from "socket.io";
import express from "express"
import cors from "cors"
import { createServer } from "http";

const PORT = 3000;
const app = express();
app.use(cors());
const server = createServer(app);

const io = new Server(server,   {
  cors: {
    origin: "*",
    methods: ["GET", "POST"],
    allowedHeaders: [],
    credentials: false,
  },
})


io.on("connection", (socket) => {
  socket.onAny((eventName, ...args)=>{
    const message = args[0]
    
    if(eventName === "user_joinRoom"){
        socket.join(message.roomId)
    } else{
        io.in(message.roomId).emit(eventName, message)
    }
  })

  socket.on('disconnect', () => {
  });
});


server.listen(PORT, () => console.log(`Server up an running on port ${PORT}`));